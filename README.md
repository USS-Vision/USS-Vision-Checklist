# USS-Vision-Checklist

Checklist for tablets.
Used to streamline often repeated processes and make them more accessible for children. 
## How to
To update the checklists, open and edit Checklisten.csv with your favorite editor. DO NOT USE ';' inside the document.
Afterwards, run the 'checklist-creation-tool' from inside the folder and choose the checklist file. The checklists should have been generated.

In order to start the webserver, enter the checklist-website folder and run serve-directory.exe when on windows and serve-directory-linux when on linux.
The device should now host the checklist website on port 8080
