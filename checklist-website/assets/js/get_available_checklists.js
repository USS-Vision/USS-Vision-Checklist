// get division id from url
var url = new URL(window.location.href);
var division = document.title;

// read checklist.json from ../assets/data/checklists.json and get the checklist corresponding to the id
document.addEventListener("DOMContentLoaded", function () {
  var output = document.getElementById("output");
  var xhr = new XMLHttpRequest();
  xhr.overrideMimeType("application/json");
  xhr.open("GET", "../../assets/data/checklists.json", true);

  xhr.onreadystatechange = function () {
    if (xhr.readyState === 4 && xhr.status === 200) {
      var data = JSON.parse(xhr.responseText);
      displayData(division, data);
    }
  };

  xhr.send();
});

function displayData(division, data) {
  var checklistContainer = document.getElementById("checklists");
  var checklistLinks = document.createElement("ul");

  // If pagetitle to lower case is in the data, then display the checklists
  if (Object.keys(data).includes(division.toLowerCase())) {
    var checklists = data[division.toLowerCase()]['checklists'];
    var index = 0;
    // Iterate over the checklists within the selected parent element
    checklists.forEach(function(checklist) {
      var checklistName = checklist.name

      var listItem = document.createElement("li");
      var link = document.createElement("a");

      link.href =
        "../checklist.html?title=" +
        document.title +
        "&division=" +
        division +
        "&id=" +
        index; // Set the link URL or leave it as "#" if not applicable
      link.textContent = checklistName; // Set the link text as the checklist title
      listItem.appendChild(link);
      checklistLinks.appendChild(listItem);
      index++;
    });
  }

  checklistContainer.appendChild(checklistLinks);
}
document.addEventListener('contextmenu', event => event.preventDefault());