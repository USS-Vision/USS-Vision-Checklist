var url_string = window.location.href; // www.test.com?title=test&division=0&id=1
var url = new URL(url_string);
var title = url.searchParams.get("title");

// set title of checklist
document.getElementById("checklist-title").innerText = title;

// read checklist.json from ../assets/data/checklists.json and get the checklist corresponding to the id
document.addEventListener("DOMContentLoaded", function () {
  var output = document.getElementById("output");
  var xhr = new XMLHttpRequest();
  xhr.overrideMimeType("application/json");
  xhr.open("GET", "../assets/data/checklists.json", true);

  xhr.onreadystatechange = function () {
    if (xhr.readyState === 4 && xhr.status === 200) {
      var data = JSON.parse(xhr.responseText);
      displayData(data);
    }
  };

  xhr.send();
});

function displayData(data) {
  // Process and display JSON data

  var division_name = url.searchParams.get("division").toLowerCase();
  var id = url.searchParams.get("id");
  var checklist = data[division_name]['checklists'][id];
  var body = document.getElementById("checklistbody");
  // set background colors
  if (division_name == 'engineering'){
    body.style.backgroundColor = '#E9A561';
  }else if(division_name == 'science'){
    body.style.backgroundColor = '#7692C6';
  }  else if (division_name == 'command'){
    body.style.backgroundColor = '#CA153D'
  } else if (division_name == 'medical'){
    body.style.backgroundColor = '#F2C9D7'
  }

  var checklistItems = Object.values(checklist);
  document.getElementById("checklist-title").innerText += ": " + checklistItems[0];
  var checklistParent = document.getElementById("checklist");
  for (var i = 0; i < checklistItems[1].length; i++) {
    // Get the checklist item
    var task = checklistItems[1][i];

    // Create the checklist item element
    var checklistItemElement = document.createElement("div");
    checklistItemElement.classList.add("checklist-item");
    // Create the checkbox and label elements
    var checklistItemCheckbox = document.createElement("input");
    checklistItemCheckbox.setAttribute("type", "checkbox");
    checklistItemCheckbox.setAttribute("id", "checklist-item-" + i);
    checklistItemCheckbox.classList.add("checklist-item-checkbox");
    var checklistItemLabel = document.createElement("label");
    checklistItemLabel.classList.add("checklist-item-label");
    checklistItemLabel.innerText = task;

    // Add the checkbox and label to the checklist item element
    checklistItemElement.appendChild(checklistItemCheckbox);
    checklistItemElement.appendChild(checklistItemLabel);

    // Add the checklist item element to the checklist
    checklistParent.appendChild(checklistItemElement);
  }
}
document.addEventListener('contextmenu', event => event.preventDefault());