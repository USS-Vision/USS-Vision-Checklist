use rfd::FileDialog;
use serde::Serialize;
use std::collections::HashMap;
#[derive(Debug, Clone, Serialize)]
struct Division {
    checklists: Vec<Checklist>,
}
#[derive(Debug, PartialEq, Clone, Serialize)]
struct Checklist {
    name: String,
    items: Vec<String>,
}

fn main() {
    let files = FileDialog::new()
        .add_filter("csv", &["csv"])
        .set_title("Wähle die .csv Datei aus.")
        .set_directory("./")
        .pick_file();

    if let Some(file) = files {
        println!("File selected: {:?}", file);
        let records = parse_csv(file.to_str().unwrap());
        write_json(records);
    } else {
        println!("User did not select any file");
    }
}

// parse csv file of the format:
// Division name, checklist name, tasks
// When multiple tasks are present, each is in a new line
// When multiple checklists are present, each is in a new line
fn parse_csv(file: &str) -> HashMap<String, Division> {
    // hashmap for divisions
    let mut divisions: HashMap<String, Division> = HashMap::new();

    let mut rdr = csv::ReaderBuilder::new()
        .delimiter(b';')
        .from_path(file)
        .unwrap();
    let mut last_division = String::new();

    for result in rdr.records() {
        let record = result.unwrap();

        // get the division name
        let division_name = record
            .get(0)
            .unwrap()
            .trim()
            .to_string()
            .to_ascii_lowercase();

        // Insert is not ""  and not already in the hashmap
        if division_name != "" {
            last_division = division_name.clone();
            if !divisions.contains_key(&division_name) {
                divisions.insert(
                    division_name.clone(),
                    Division {
                        checklists: Vec::new(),
                    },
                );
            }
        }

        // get the checklist name
        let checklist_name = record
            .get(1)
            .unwrap()
            .trim()
            .to_string()
            .to_ascii_lowercase();

        // Insert if not "" and not already in the hashmap
        if checklist_name != "" {
            if !divisions
                .get(&last_division)
                .unwrap()
                .checklists
                .contains(&Checklist {
                    name: checklist_name.clone(),
                    items: Vec::new(),
                })
            {
                divisions
                    .get_mut(&last_division)
                    .unwrap()
                    .checklists
                    .push(Checklist {
                        name: checklist_name.clone(),
                        items: Vec::new(),
                    });
            }
        }

        // get the tasks
        let task = record.get(2).unwrap().to_string();
        // if not empty: insert into the checklist
        if task != "" {
            divisions
                .get_mut(&last_division)
                .unwrap()
                .checklists
                .last_mut()
                .unwrap()
                .items
                .push(task);
        }
    }
    divisions
}

fn write_json(records: HashMap<String, Division>) {
    // write hashmap to json file
    let mut  file = std::fs::File::create("checklist-website/assets/data/checklists.json");
    if let Err(e) = &file {
        println!("File not found in default location.");

        let mut target_path = String::from("./");

        // check if target_path + assets/data/checklists.json exists. Repeat until it does
        while file.is_err() {
            // get target path from user
            let folder = FileDialog::new().set_directory(&target_path)
            .set_title("Wähle den 'checklist-website' Ordner aus.")
            .pick_folder();
            if let Some(folder) = folder {
                println!("Folder selected: {:?}", folder);
                target_path = folder.to_str().unwrap().to_string();
                file = std::fs::File::create(target_path.to_owned() + "/assets/data/checklists.json");
            } else {
                println!("User did not select a valid folder");
                println!("Exiting program");
                std::process::exit(1);
            }
        }
        // create file at target path
    }
   let mut file = file.unwrap();

    serde_json::to_writer_pretty(&mut file, &records).unwrap();
}
